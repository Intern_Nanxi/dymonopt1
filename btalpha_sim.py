import os
import pandas as pd
from btserver_lib import BtServer
from typing import Dict, Optional

DATA_CACHE_STR = """
Available dataframes can be viewed here: http://10.5.0.12:5555/cache_dir
Example: to get adj_low.dataframe, call <BTAlpha_Sim>.df.adj_low
"""

class BtDataFrames:
    def __init__(self, api : BtServer, cache : str):
        self.api = api
        self.cache = cache
        self._dfs : Dict[str, pd.DataFrame] = {}
            
    def __getattr__(self, df_name: str) -> Optional[pd.DataFrame]:
        try:
            return self._dfs[df_name]
        except:
            df_str = f'{df_name}.dataframe'
            try:
                print(f'Initial Request for <{df_str}> -- Fetching Data ...')
                self._dfs[df_name] = self.api.getCache(self.cache, df_str)
                return self._dfs[df_name]
            except:
                print(f'\nERROR: dataframe not found: {df_str}\n')
                print(DATA_CACHE_STR)
                return None

class BtSeries:
    def __init__(self, api : BtServer, cache : str):
        self.api = api
        self.cache = cache
        self._srs : Dict[str, pd.Series] = {}
            
    def __getattr__(self, sr_name: str) -> Optional[pd.Series]:
        try:
            return self._srs[sr_name]
        except:
            sr_str = f'{sr_name}.series'
            try:
                print(f'Initial Request for <{sr_str}> -- Fetching Data ...')
                self._srs[sr_name] = self.api.getCache(self.cache, sr_str)
                return self._srs[sr_name]
            except:
                print(f'\nERROR: series not found: {sr_str}\n')
                print(DATA_CACHE_STR)
                return None

class BtAlpha_Sim():
    def __init__(self, cache : str):
        self.api : BtServer = BtServer()
        self.cache : str = cache
        self.df : BtDataFrames = BtDataFrames(api=self.api, cache=self.cache)
        self.sr : BtSeries = BtSeries(api=self.api, cache=self.cache)
        self.delay : int = 1
        print(DATA_CACHE_STR)

    def run_alpha(self):
        """
        *** This is an abstract method: Please implement in subclass ***
        """
        raise NotImplementedError("Required method: Please create alpha")

    
    def _check_valid(self, df_alpha : pd.DataFrame) -> pd.DataFrame:
        df_alpha = df_alpha.shift(periods=self.delay).fillna(0)
        df_alpha = df_alpha*self.df.valid
        df_alpha = df_alpha.dropna(how='all')
        return df_alpha

    def _evaluate_server(self):
        '''
        Returns: zipfile
        '''
        self.alpha = self.run_alpha()
        try:
            self.alpha = self._check_valid(self.alpha)
            print(f'Analyzing Alpha...')
            result = self.api.evaluate_alphadf(self.alpha)
            return result
        except AttributeError:
            return 'Error: Alpha not created'

    def evaluate(self):
        zip = self._evaluate_server()
        zip.extractall(self.result_folder)
        print('\nCOMPLETE: Sim Results printed to ', self.result_folder, ' folder')
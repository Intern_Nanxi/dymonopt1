import pandas as pd
import numpy as np
# import matplotlib

import rbfopt
import rbfopt.rbfopt_black_box as bb
import sys
sys.path.insert(0, '/opt/conda/envs/opt/lib/python3.8/site-packages')
settings = rbfopt.RbfoptSettings(minlp_solver_path='/opt/conda/envs/opt/lib')

#INPUT PARAMS
# FAST_HL = 120 # 5 to 1440
# SLOW_HL = 2880 # FAST_HL to 7*24*60 
# EDGE_REQ = 0.01 # 0 to 0.05 

n_factor : int = 3
low_bound : list = [5, 5, 0.001]
up_bound : list = [1440, 7200, 0.05]
var_type : list = ['I', 'I', 'R']
    
fast_hl, slow_hl, edge_required, sharpe_list, returns_list = ([] for i in range(5))
print(f'factor numbers:{n_factor}, upper bound:{up_bound}, lower bound:{low_bound}') 

def target_position(x, edge):
    # edge required
    edge_req = edge
    
    if x > edge_req:
        return 1
    elif x < -edge_req:
        return -1
    else:
        return 0
    
def evaluate(point : list):
    btc_prices = pd.read_csv('btc_march_april.csv')
    btc_prices['open_change'] = np.log(btc_prices['open_pr'] / btc_prices['open_pr'].shift(1))
#     print(btc_prices)
    point[2] = round(point[2], 2)
    print("edge_req, FAST_HL and SLOW_HL", point[2], point[0], point[1])
    if point[1] <= point[0]:
        point[1] = point[0] + (7200 - point[0]) * (point[1] - 5) / (7200 - 5)
        point[1] = round(point[1])
    print("FAST_HL and SLOW_HL", point[0], point[1])
        
    btc_prices['ema_fast'] = btc_prices['open_pr'].ewm(halflife=point[0]).mean()
    btc_prices['ema_slow'] = btc_prices['open_pr'].ewm(halflife=point[1]).mean()        
#     btc_prices[['ema_fast', 'ema_slow']].plot(figsize=(20,10))

    btc_prices['edge'] = np.log(btc_prices['ema_fast'] / btc_prices['ema_slow'])
#     btc_prices['edge'].plot(figsize=(20,10))

    btc_prices['position'] = btc_prices['edge'].apply(target_position, edge=point[2])
#     btc_prices['position'].plot(figsize=(20,10))

    btc_prices['port_ret'] = btc_prices['open_change'] * btc_prices['position']
    btc_prices['cum_port_ret'] = btc_prices['port_ret'].cumsum()
#     print('btc_prices of cum_port_ret', btc_prices['cum_port_ret'].std())
#     btc_prices['cum_port_ret'].plot(figsize=(20,10))

    #SHARPE
    if btc_prices['cum_port_ret'].std() == 0:
        btc_sharpe = 0
    else:
        btc_sharpe = btc_prices['cum_port_ret'].mean() / btc_prices['cum_port_ret'].std()
    sharpe_list.append(btc_sharpe)
    #Total Returns
    btc_totalreturn = btc_prices['cum_port_ret'].iloc[-1]
    returns_list.append(btc_totalreturn)
    
    fast_hl.append(point[0])
    slow_hl.append(point[1])
    edge_required.append(point[2])

    return (btc_sharpe*(-1))

if __name__ == '__main__':
    # parse the args
    
    bb = rbfopt.RbfoptUserBlackBox(n_factor, low_bound, up_bound,
                                    np.array(var_type), evaluate)
    settings = rbfopt.RbfoptSettings(max_evaluations=500)
    alg = rbfopt.RbfoptAlgorithm(settings, bb)
    objval, x, itercount, evalcount, fast_evalcount = alg.optimize()
    print('objective value at x: ', objval, x)
    datalist = [fast_hl, slow_hl, edge_required, sharpe_list, returns_list]
    output_df = pd.DataFrame(datalist).transpose()
    # out_scatter = pd.plotting.scatter_matrix(output_df)
    # plt.savefig(r"scatter_fig.png")
    output_df.columns = ['FAST_HL', 'SLOW_HL', 'EDGE_REQUIRED', 'sharpe', 'returns']
    output_df.to_csv('BTC_output_df.csv')
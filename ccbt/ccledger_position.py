import pdb

from datetime import datetime, timedelta, time
from lib_DI_SQLTools import SQLTools
import pandas as pd
import numpy as np
from ccom import CCOrder, CCOrderManager
from ccdata import CCSymbol, CCSymbolFamily
import csv
from csv import DictReader
import os

MARK_TIMES = [1, 5, 10, 30, 60, 180, 360, 720, 1440, 2880, 4320, 10080, 20160, 40320]

class CCLedger:

    def __init__(self):
        self.fill_list = []
        #dit of positions with their size key symbol, value size
#         self.load_positions_from_csv()
        self.pos : Dict[str, pd.DataFrame] = {}
#         self.order_time = []
        self.filltime = []
        self.symbol = []
        self.size = []
        self.price = []
#         self.aa = CCTrade(CCOrder())    

    def load_position_from_csv(self, file):
        if os.path.exists(file):
            df = pd.read_csv(file,index_col=0)
#             with open('position.csv', newline='') as csv_file:
#                 csv_reader = csv.DictReader(csv_file)
#                 header_name = csv_reader.fieldnames
#                 print(header_name)
#                 for row in csv_reader:
#                     for cols in header_name:
#                         self.pos[cols] = [row[cols]]
            return df
        else:
            return pd.DataFrame({})


    def insert_fills(self, fill_list):
        df_old = self.load_position_from_csv('position.csv')
        
        self.fill_list = self.fill_list + fill_list

        #UPDATE Position
        for filllist in fill_list:
#             filllist.list()
            fill = filllist[-1]
# #             self.order_time.append(fill.order.order_datetime.strftime('%Y-%m-%d %H:%M'))
#             for key in position_old:
#                 if key == 'filltime':
#                     position_old[key].append(fill.fill_time)
#                 elif key == fill.order.symbol.symbol+' '+fill.order.symbol.market:
#                     position_old[key].append(float(position_old[key][-1])+float(fill.fill_size))            
            self.filltime.append(fill.fill_time)
            self.symbol.append(fill.order.symbol.symbol+' '+fill.order.symbol.market)
            self.size.append(fill.fill_size)
#             print(fill.order.symbol.price_at(fill.fill_time))
            
        df1 = pd.DataFrame(columns=list(dict.fromkeys(self.symbol)), index=list(dict.fromkeys(self.filltime)))

        for i in range(len(fill_list)):
            df1.at[self.filltime[i], self.symbol[i]] = self.size[i]
#         self.pos = {'filltime': self.filltime, 'symbol': self.symbol, 'size': self.size}
#         df1 = pd.DataFrame(self.pos).groupby(['filltime', 'symbol'],as_index=False)['size'].sum().unstack()
#         df1=df.pivot_table(index='datetime', columns='symbol', values='size')
        df = pd.concat([df_old, df1], axis=0)

        df.fillna(0, inplace=True)
        for i in range(df_old.shape[0], df.shape[0]):
            if 0!=i:
                df.iloc[i] = df.iloc[i] + df.iloc[i-1]
            else:
                continue
        df.to_csv('position.csv', index_label='datetime')

    def analyze_position(self):
        df_position = pd.read_csv('position.csv', index_col=0)
        symbol_list = list(df_position)
        df_price = pd.DataFrame(columns=symbol_list)
        df_position.index = pd.to_datetime(df_position.index)
        start_time = df_position.index[0]
#         start_time : datetime = datetime(2021,4,4)
        end_time : datetime = datetime(2021,4,20)
        for index, row in df_position.iterrows():
            for symbol in symbol_list:
                symbol_class = CCSymbol(symbol.split(' ')[0], symbol.split(' ')[1])   #symbol and market
                row[symbol] = symbol_class.price_at(index)
            df_price = df_price.append(row)

        df_range_prices = pd.DataFrame(columns=symbol_list)
        for symbol in symbol_list:
            symbol_class = CCSymbol(symbol.split(' ')[0], symbol.split(' ')[1])
            df_range_prices[symbol] = symbol_class.prices(start_time, end_time, 1440).iloc[:,0]
        df_range_prices.index = pd.to_datetime(df_range_prices.index)

        new_position = []
        new_position_date = []
        for date_range_index in df_range_prices.index:
            for i in range(len(df_position.index)-1):
                if (df_position.index[i] < date_range_index) & (date_range_index < df_position.index[i+1]): 
                    new_position_series = df_position.iloc[i]
                    new_position_date.append(date_range_index)
                    new_position.append(new_position_series)
            if date_range_index > df_position.index[-1]:
                new_position_series = df_position.iloc[-1]
                new_position_date.append(date_range_index)
                new_position.append(new_position_series)
        line = pd.to_datetime(new_position_date, format="%Y-%m-%d %H:%M:%S")
        df_range_position = pd.DataFrame(new_position, columns=symbol_list, index=line)
        df_position_new = pd.concat([df_position, pd.DataFrame(df_range_position)], ignore_index=False)
        df_position_new = df_position_new.sort_index()
        df_position_new.to_csv('position_new.csv', index_label='datetime')
        df_price_new = pd.concat([df_price, df_range_prices]).drop_duplicates()
        df_price_new = df_price_new.sort_index()
        df_price_new.to_csv('position_price.csv', index_label='datetime')   
        
        df_diff = df_price_new.diff()
        df_diff.fillna(0, inplace=True)
        df_diff.to_csv('price_diff.csv', index_label='datetime')
        
        df_pnl = pd.DataFrame(df_position_new.values*df_diff.values, columns=df_position_new.columns, index=df_position_new.index)
        df_pnl.fillna(0, inplace=True)
        df_pnl.to_csv('pnl.csv', index='datetime')
    
    def analyze_pnl(self, file):
        if os.path.exists(file):
            df_pnl_file = pd.read_csv(file, index_col=0) 
#             df_pnl_file['datetime'] = df_pnl_file.index
            df_pnl_file['Date'] = pd.to_datetime(df_pnl_file.index, format="%Y-%m-%d %H:%M")
            for k in range(df_pnl_file.shape[0]-1):
                if df_pnl_file['Date'][k+1].day-df_pnl_file['Date'][k].day == 1:
                    end_day = datetime.combine(df_pnl_file['Date'][k+1].date(), time.min)
                    print(end_day)
                    new_row = df_pnl_file.iloc[:(k+1)].sum()
                    print(new_row)
                    new_df = pd.DataFrame([new_row],index=[end_day])
                    df_pnl_file = pd.concat([df_pnl_file.iloc[:(k+1)], new_df, df_pnl_file.iloc[(k+1):]])
            df_pnl_file = df_pnl_file.iloc[:, :-1]
            df_pnl_file.to_csv('pnl_endofday.csv', index='datetime')
#             df_pnl_file = df_pnl_file.sort_index()  # sorting by index
#         df_pnl_file.index = pd.to_datetime(df_pnl_file.index)
#         print(df_pnl_file.index.dt.day.tail(1))
# df['timestamps'] = pd.to_datetime(df['timestamps'])
# df['hour'] = df['timestamps'].dt.hour
# df.groupby(df['timestamps'].dt.day).agg({'hour': ['min', 'max', 'mean']}) \
#                                    .stack(level=0).droplevel(1)
        
    def analyze_fills(self):
        self.pnl = {}
        self.returns = {}
            
        for fill in self.fill_list:
            pnl = {}
            returns = {}
            

            for minutes in MARK_TIMES:
                try:
                    pnl[minutes] = fill.order.symbol.price_at(fill.fill_time + timedelta(minutes=minutes)) -  fill.fill_price
                    returns[minutes] = pnl[minutes]/fill.fill_price
                except Exception as e:
                    pass

            print ('\n')
            
            
            self.pnl[fill] = pnl
            self.returns[fill] = returns
            
#             for minutes in returns:
#                 print (f'{minutes} -- {returns[minutes]:.4f}')
            
    def print_returns(self):
        for fill in self.returns:
            print(f'TRADE: \n{fill}')
            print ('RETURNS:')
            for mark in self.returns[fill]:
                print (f'{mark}:{self.returns[fill][mark]:.4f}', end=', ')
            print ('\n')

if __name__ == '__main__':
    
    om = CCOrderManager('mystrat')
    
    om.create_order(CCSymbol('BTC/USDT', 'SPOT', 'SPOT'), 'MKT', 'B', 1.2, None)
    fl1 = om.send_orders(fill_time = datetime(2021,4,4))
    
    om.create_order(CCSymbol('ETH/USDT', 'SPOT', 'SPOT'), 'MKT', 'B', 2, None)
    fl2 = om.send_orders(fill_time = datetime(2021,4,4,1))
    
    om.create_order(CCSymbolFamily('BTC/USDT').linear_perp, 'MKT', 'B', 2.5, None)
    fl3 = om.send_orders(fill_time = datetime(2021,4,4,1,2))
    
    om.create_order(CCSymbolFamily('ETH/USDT').linear_perp, 'MKT', 'S', 1.2, None)
    fl4 = om.send_orders(fill_time = datetime(2021,4,4,1,5))
    
    om.create_order(CCSymbolFamily('ETH/USDT').LINEAR_FRONT, 'MKT', 'B', 1.2, None)
    fl5 = om.send_orders(fill_time = datetime(2021,4,4,11,5))
    
    om.create_order(CCSymbolFamily('ETH/USDT').INVERSE_BACK, 'MKT', 'B', 0.2, None)
    fl6 = om.send_orders(fill_time = datetime(2021,4,5,23,55))

    
    ledger = CCLedger()
#     fl = fl1 + fl2 + fl3
#     ledger.insert_fills([fl1,fl2,fl3])
#     ledger.insert_fills([fl1, fl2, fl3, fl4, fl5, fl6])
    ledger.analyze_position()
#     ledger.analyze_pnl('pnl.csv')
    
#     ledger.analyze_fills()
#     ledger.print_returns()

#     _ccom.create_order('ETH/USDT', 'LINEAR', 'Binance', 'MKT', 'S', 3, None)
#     _ccom.create_order('SHIBUSD', 'SPOT', 'Binance', 'MKT', 'B', 0.2, None)
#     _ccom.create_order('BTCUSD', 'SPOT', 'Binance', 'MKT', 'B', 1.2, None)
#     print(str(o1))
#     _ccom.print_orders()


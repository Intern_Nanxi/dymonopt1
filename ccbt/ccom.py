import cclib
from typing import Optional, List
from datetime import datetime
from ccdata import CCSymbol


class CCOrder:
    
    class OrderCreationError(Exception):
        pass
    
    """
    For now ... 
    side = ['B', 'S']
    order_types = ['MKT', 'LMT']
    """
    
    def __init__(self, symbol : CCSymbol, #symbol : str, market : str, exchange : str, 
                 order_type : str, side : str, size : float, price : Optional[float], 
                 fair : Optional[float] = None, notes : str = None, order_datetime : datetime = None):
        
        self.symbol : CCSymbol = symbol
#         self.symbol : str = symbol
#         self.market : str= market
#         self.exchange : str = exchange
        
        self.order_type : str = order_type
        self.side : str = side
            
        if order_type == 'MKT':
            if price is not None:
                raise OrderCreationError
            self.price : Optional[float] = None

        if order_type == 'LMT':
            if price is None:                
                raise OrderCreationError
            self.price : Optional[float] = price
                
        self.size : float = size
        
        self.fair : str = fair
        self.notes : str= notes
            
        if order_datetime is None:
            self.order_datetime : datetime = datetime.utcnow()
        else:
            self.order_datetime : datetime = order_datetime
                
                

    def __str__(self):
        if self.order_type == 'MKT':
            return f'{self.order_datetime.strftime("%Y%m%d")}, {self.order_datetime.strftime("%H%M%S.%f")}, {self.symbol}, {self.side}, {self.size}, {self.order_type}, {self.fair}, {self.notes}'
#             return f'{self.order_datetime.strftime("%Y%m%d")}, {self.order_datetime.strftime("%H%M%S.%f")}, {self.symbol}, {self.market}, {self.exchange}, {self.side}, {self.size}, {self.order_type}, {self.fair}, {self.notes}'
        
    def __repr__(self):
        return self.__str__
    
    
class CCFill:

    def __init__(self, order : CCOrder, fill_price : float, fill_size : float, fill_time : datetime):
        self.order = order
        self.fill_price = fill_price
        self.fill_size = fill_size
        self.fill_time = fill_time

    def __str__(self):
        return f'{self.order}, {self.fill_price}, {self.fill_size}, {self.fill_time}'
    
    def __repr__(self):
        return self.__str__
    
    
class CCOrderManager:
    """
    Each Strategy will have its own Order Manager (OM). 
    OM interfaces with the Order Gateway / Fill Simulator through a flat file.
    This way, every outbound order is logged.
    """

    def __init__(self, strat: str, gateway = None):
        self.strat : str = strat
        self.filename : str = f'{self.strat}_{cclib.today_str()}.ord'
        self.fileptr : _io.TextIOWrapper = open (self.filename, 'a', buffering=1)
        self.order_list : List[CCOrder] = []
        self.gateway = gateway
            
    def create_order(self, symbol, #: str, market : str, exchange : str, 
                 order_type : str, side : str, size : float, price : Optional[float], 
                 fair : Optional[float] = None, notes : str = None, order_datetime : datetime = None):
        
        _order = CCOrder(symbol, #symbol, market, exchange, 
                 order_type , side , size, price, 
                 fair, notes, order_datetime)
        
        self.order_list.append(_order)
        self.fileptr.write(_order.__str__())
        self.fileptr.write('\n')

    def print_orders(self):
        with open(self.filename, 'r') as f:
            for line in f:
                print (line.strip())
        
    
    def send_orders(self, fill_time = None) -> List:
        # this should eventually be offloaded to fill sim
        
        if fill_time is None:
            fill_time = datetime.utcnow()
        fill_list = []
        if self.gateway is None:
            for order in self.order_list:
                fill_list.append(CCFill(order, order.symbol.price_at(fill_time), order.size, fill_time))       
        return fill_list
            
    



def test1():
    print('>>>>TEST1<<<')
    _ccom = CCOrderManager('mystrat')
    _ccom.create_order(CCSymbol('BTC/USDT', 'SPOT', 'SPOT'), 'MKT', 'B', 1.2, None)
#     _ccom.create_order('ETH/USDT', 'LINEAR', 'Binance', 'MKT', 'S', 3, None)
#     _ccom.create_order('SHIBUSD', 'SPOT', 'Binance', 'MKT', 'B', 0.2, None)
#     _ccom.create_order('BTCUSD', 'SPOT', 'Binance', 'MKT', 'B', 1.2, None)
#     print(str(o1))
#     _ccom.print_orders()
    ol = _ccom.send_orders(fill_time = datetime(2021,4,4))

    
    for oo in ol:
        print (oo)
        
if __name__ == '__main__':
    test1()
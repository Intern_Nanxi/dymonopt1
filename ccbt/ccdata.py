from datetime import datetime
from lib_DI_SQLTools import SQLTools
import pandas as pd
import numpy as np

# This should be looked up dynamically from a lookup table
MARKET_MAP = {'SPOT' : 'crypto.ccxt_spot_bars'
             ,'LINEAR' : 'crypto.ccxt_usdt_bars'
             , 'INVERSE' : 'crypto.ccxt_coinm_bars' 
             }

# This should be looked up dynamically from a lookup table
FAMILY_MAP = {'BTC/USDT':{'SPOT':'BTC/USDT', 'INVERSE_PERP':'BTC/USD', 'INVERSE_FRONT':'BTCUSD_210625', 'INVERSE_BACK':'BTCUSD_210924', 'LINEAR_PERP':'BTC/USDT', 'LINEAR_FRONT':'BTCUSDT_210625'}
              ,'ETH/USDT':{'SPOT':'ETH/USDT', 'INVERSE_PERP':'ETH/USD', 'INVERSE_FRONT':'ETHUSD_210625', 'INVERSE_BACK':'ETHUSD_210924', 'LINEAR_PERP':'ETH/USDT', 'LINEAR_FRONT':'ETHUSDT_210625'                  
              }
                          
             }



class CCSymbol:
    
    def __init__(self, symbol : str, market : str, family_type : str = ''):
        self.symbol : str = symbol
        self.family_type : str = family_type
        self.market : str = market
        self.market_table : str = MARKET_MAP[self.market]
            
        self.sql_str = f"SELECT timestamp_utc, <DATA> FROM {self.market_table} WHERE symbol = '{self.symbol}' AND timestamp_utc >= '<START>' AND timestamp_utc <= '<END>'"

        self.sql_str_interval = f"select timestamp_utc, <DATA> from (select * , rank() over (partition by symbol, contract_type order by timestamp_utc asc) as time_rank from {self.market_table} where symbol = '{self.symbol}' and timestamp_utc >= '<START>' and timestamp_utc <= '<END>' ) as ranked_table where (time_rank % <INTERVAL> = 1)"        

        self.sql_str_interval_include = f"select timestamp_utc, <DATA> from (select * , rank() over (partition by symbol, contract_type order by timestamp_utc asc) as time_rank from {self.market_table} where symbol = '{self.symbol}' and timestamp_utc >= '<START>' and timestamp_utc <= '<END>' ) as ranked_table where (time_rank % <INTERVAL> = 1 or timestamp_utc = '<END>')"        
        
    def __str__(self):
        return f'{self.symbol}, {self.family_type}, {self.market}, {self.market_table}'
    
    def __repr__(self):
        return self.__str__
    
    def returns(self, from_time : datetime, to_time : datetime
                , interval : int = 1, include_end : bool = True
               ) -> pd.DataFrame:
        
        df = self.prices(from_time, to_time, interval, include_end)
        df = df/df.iloc[0]
        df = df.rename(columns={'open_pr':'ret'})

        return df
    
    def prices(self, from_time : datetime , to_time : datetime
               , interval : int = 1, include_end : bool = True
              ) -> pd.DataFrame:
        
        if interval > 1:
            if include_end:
                sql_str = self.sql_str_interval_include
            else:
                sql_str = self.sql_str_interval
            sql_str = sql_str.replace('<INTERVAL>', f'{interval}')      
        else:
            sql_str = self.sql_str
            
        sql_str = sql_str.replace('<DATA>', 'open_pr')      
        sql_str = sql_str.replace('<START>', from_time.strftime('%Y-%m-%d %H:%M'))
        sql_str = sql_str.replace('<END>', to_time.strftime('%Y-%m-%d %H:%M'))      

        df = SQLTools(sql_query=sql_str).query()
        df = df.set_index('timestamp_utc')
        df = df.rename(columns={'open_pr':'px'})
        return df  
    
    def price_at(self, at_time : datetime) -> float:
        
        sql_str = self.sql_str        
        sql_str = sql_str.replace('<DATA>', 'open_pr')      
        sql_str = sql_str.replace('<START>', at_time.strftime('%Y-%m-%d %H:%M'))
        sql_str = sql_str.replace('<END>', at_time.strftime('%Y-%m-%d %H:%M'))      

        df = SQLTools(sql_query=sql_str).query()
        return df['open_pr'][0]

    def returns_between(self, from_time : datetime, to_time : datetime) -> float:
        
        start_price = self.price_at(from_time)
        end_price = self.price_at(to_time)
        
        return end_price / start_price
    
    def volume_between(self, from_time: datetime, to_time: datetime) -> float:
        
        sql_str = f"SELECT sum([volume]) as volume FROM {self.market_table} where symbol = '{self.symbol}' and timestamp_utc >= '<START>' and timestamp_utc <= '<END>'"
        sql_str = sql_str.replace('<START>', from_time.strftime('%Y-%m-%d %H:%M'))
        sql_str = sql_str.replace('<END>', to_time.strftime('%Y-%m-%d %H:%M'))      
        
        df = SQLTools(sql_query=sql_str).query()
        
        return df['volume'][0]
    
    def volumes(self, from_time : datetime , to_time : datetime
               , interval : int = 1, drop_end : bool = True) -> pd.DataFrame:
        
        if interval > 1:
            sql_str = f"select timestamp_utc, vol from (select *, SUM(volume) over (PARTITION by FLOOR(time_rank / {interval})) as vol from (select * , rank() over (partition by symbol, contract_type order by timestamp_utc asc) as time_rank from {self.market_table} where symbol = '{self.symbol}' and timestamp_utc >= '<START>' and timestamp_utc <= '<END>' ) as ranked_table ) as tt where time_rank % {interval} = 1 order by timestamp_utc asc"
        else:
            sql_str = f"select timestamp_utc, vol from (select *, SUM(volume) over (PARTITION by FLOOR(time_rank / {interval})) as vol from (select * , rank() over (partition by symbol, contract_type order by timestamp_utc asc) as time_rank from {self.market_table} where symbol = '{self.symbol}' and timestamp_utc >= '<START>' and timestamp_utc <= '<END>' ) as ranked_table ) as tt order by timestamp_utc asc"
            

        sql_str = sql_str.replace('<START>', from_time.strftime('%Y-%m-%d %H:%M'))
        sql_str = sql_str.replace('<END>', to_time.strftime('%Y-%m-%d %H:%M'))      
        
        df = SQLTools(sql_query=sql_str).query()
        df = df.set_index('timestamp_utc')
        
        if drop_end:
            df = df[:-1]
        return df

    def dollar_volumes(self, from_time : datetime , to_time : datetime
               , interval : int = 1, drop_end : bool = True) -> pd.DataFrame:
        
                    
        df = self.volumes(from_time, to_time, interval, drop_end) 
        
        if self.market == 'INVERSE':
            df_prices = self.prices(from_time, to_time, interval, not drop_end)
            df['vol'] = df['vol'] * df_prices['px']

        return df

    
class CCSymbolFamily:
    
    def __init__(self, spot_symbol):
        #this should be dynamic from SQL Table (SecMaster)
        
        self.family = {}
        self.load_family(FAMILY_MAP[spot_symbol])
        self.default_start = datetime(2021,4,4)
        self.default_end = datetime(2021,4,5)
                

    def __getattr__(self, fam_type : str) -> CCSymbol:
        return self.family[fam_type.upper()]        
        
    def load_family(self, family_map):
        
        for _type in family_map:
            symbol = family_map[_type]
            market = _type.split('_')[0]
            
            self.family[_type] = CCSymbol(symbol, market, _type)

        
    def family_returns(self, start_time: datetime, end_time : datetime
                           , interval : int = 1, include_end : bool = True
                          ) -> pd.DataFrame:

        
        list_df = []
        
        for inst in self.family.values():
            
            _sym = f'{inst.family_type}'
            _df = inst.returns(start_time, end_time, interval, include_end)
            _df = _df.rename(columns={'px':_sym})
            list_df.append(_df)
            
        df = pd.concat(list_df, axis=1).sort_values('timestamp_utc')
        return df

    def family_prices(self, start_time: datetime, end_time : datetime
                           , interval : int = 1, include_end : bool = True
                          ) -> pd.DataFrame:

        list_df = []
        
        for inst in self.family.values():
            
            _sym = f'{inst.family_type}'
            _df = inst.prices(start_time, end_time, interval, include_end)
            _df = _df.rename(columns={'px':_sym})
            list_df.append(_df)
            
        df = pd.concat(list_df, axis=1).sort_values('timestamp_utc')
        return df
    

    def family_premiums(self, start_time: datetime, end_time : datetime
                           , interval : int = 1, include_end : bool = True
                          ) -> pd.DataFrame:
        

        df_prices = self.family_prices(start_time, end_time, interval, include_end)
        
        df = pd.DataFrame(index=df_prices.index)
        
        for col in df_prices.drop('SPOT',axis=1).columns:
            df[col] = df_prices[col] - df_prices['SPOT']
        
        return (df)
        
    def family_volumes(self, start_time: datetime, end_time : datetime
                           , interval : int = 1, drop_end : bool = True
                          ) -> pd.DataFrame:

        
        list_df = []
        
        for inst in self.family.values():
            
            _sym = f'{inst.family_type}'
            _df = inst.volumes(start_time, end_time, interval, drop_end)
            _df = _df.rename(columns={'vol':_sym})
            list_df.append(_df)
            
        df = pd.concat(list_df, axis=1).sort_values('timestamp_utc')
        return df            

    def family_dollar_volumes(self, start_time: datetime, end_time : datetime
                           , interval : int = 1, drop_end : bool = True
                          ) -> pd.DataFrame:

        
        list_df = []
        
        for inst in self.family.values():
            
            _sym = f'{inst.family_type}'
            _df = inst.dollar_volumes(start_time, end_time, interval, drop_end)
            _df = _df.rename(columns={'vol':_sym})
            list_df.append(_df)
            
        df = pd.concat(list_df, axis=1).sort_values('timestamp_utc')
        return df      

def example():

    start_time : datetime = datetime(2021,4,4)
    end_time : datetime = datetime(2021,5,4)

    btcfam = CCSymbolFamily('BTC/USDT')
    df_btc = btcfam.family_prices(start_time, end_time, 1440)
    
#     ethfam = CCSymbolFamily('ETH/USDT')
#     df_eth = ethfam.get_family_returns(start_time, end_time, 1440)
    
    print (f"\nBTC RETURNS\n")
    df_btc.ffill().plot()
#     print (f"\nETH RETURNS\n")
#     df_eth.ffill().plot()
    

def example2():
#     btcperp = CCSymbol('BTC/USDT', 'FUT_USDT')
    btcperp = CCSymbol('BTC/USDT', 'SPOT')
    start_time : datetime = datetime(2021,1,1,1)
    end_time : datetime = datetime(2021,1,1,1,10)
    df = btcperp.prices(start_time, end_time, 4)    
    print(df)
    

def example3():
    btcperp = CCSymbol('BTC/USDT', 'SPOT')
    mark_time : datetime = datetime(2021,1,1,1)
    
    print (f'MARK PRICE = {btcperp.price_at(mark_time)}')

            
def example4():

    start_time : datetime = datetime(2021,4,4)
    end_time : datetime = datetime(2021,4,20)

    btcfam = CCSymbolFamily('BTC/USDT')
    df = btcfam.family_premiums(start_time, end_time, 1440)
    df[['INVERSE_FRONT', 'LINEAR_FRONT']].plot()
    df[['INVERSE_PERP', 'LINEAR_PERP']].plot()
    (df['INVERSE_FRONT'] - df['LINEAR_FRONT']).plot()
    (df['INVERSE_PERP'] - df['LINEAR_PERP']).plot()
    
#     ethfam = CCSymbolFamily('ETH/USDT')
#     df_eth = ethfam.get_family_returns(start_time, end_time, 1440)
    
#     print (f"\nBTC RETURNS\n")
#     df_btc.ffill().plot()
#     print (f"\nETH RETURNS\n")
#     df_eth.ffill().plot()
    
def example5():
    btcperp = CCSymbol('BTC/USDT', 'SPOT')
    start_time : datetime = datetime(2021,1,1,1)
    end_time : datetime = datetime(2021,1,1,1,10)
    px_ret = btcperp.returns_between(start_time, end_time)
    print(px_ret)

if __name__ == '__main__':
    pass
    example()
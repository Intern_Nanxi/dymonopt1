# import argparse

import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from btalpha_sim import BtAlpha_Sim


class AlphaSample(BtAlpha_Sim):
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs) 

        # Nancy to change the output folder name every time
        self.result_folder = 'result'       

    
    def run_alpha(self, point : list):
        '''                                                                                                                                            
        write alpha and assign                                                                                                                         
        '''
        # Define factors
        print("point in run_alpha", point)
        t1 = int(point[0])
        t2 = int(point[1])
        t3 = int(point[2])
        q1 = point[3]
        q2 = point[4]

        # Define Alpha                                                                                                                     
        alpha1 = self.df.open.apply(lambda x: -1 * self.df.open[x.name].rolling(t1).corr(self.df.vwap[x.name]), axis=0)
        alpha2 = self.df.open.apply(lambda x: -1 * self.df.open[x.name].rolling(t2).corr(self.df.vwap[x.name]), axis=0)
        alpha3 = self.df.open.apply(lambda x: -1 * self.df.open[x.name].rolling(t3).corr(self.df.vwap[x.name]), axis=0)

        alpha = q1 * alpha1 + q2 * alpha2 + (1-q1-q2) * alpha3
           
        return alpha

    def _evaluate_server(self):
        '''
        Returns: zipfile
        '''
#         self.alpha = self.run_alpha()
        try:
            self.alpha = self._check_valid(self.alpha)
            print(f'Analyzing Alpha...')
            result = self.api.evaluate_alphadf(self.alpha)
            return result
        except AttributeError:
            return 'Error: Alpha not created'


        
# if __name__ == '__main__':
#     # parse the args
#     alpha = AlphaSample(cache='JP_cache')
# #     alpha.result_folder = 'results'
#     alpha.evaluate()
    


    
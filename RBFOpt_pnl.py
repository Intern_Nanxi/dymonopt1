import pdb

from numpy import arange
import itertools
import pandas as pd
import ast
from btalpha_try import AlphaSample

import sys
sys.path.insert(0, '/opt/conda/envs/opt/lib/python3.8/site-packages')

import rbfopt
import numpy as np
import rbfopt.rbfopt_black_box as bb

settings = rbfopt.RbfoptSettings(minlp_solver_path='/opt/conda/envs/opt/lib')
 

class AlphaOpt(AlphaSample):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)              
    
    def input_data(self):
        self.n_factor : int = 5
        self.low_bound : list = [10, 10, 10, 0.1, 0.1]
        self.up_bound : list = [15, 15, 15, 0.5, 0.5]
        self.var_type : list = ['I', 'I', 'I', 'R', 'R']
        print(f'factor numbers:{self.n_factor}, upper bound:{self.up_bound}, lower bound:{self.low_bound}') 

        self.factor : Dict[str, pd.DataFrame] = {}
        for i in range(0, self.n_factor):
            self.factor["factor" + str(i)] = []
        self.factor["PnL"] = []
        
    def evaluate(self, point : list):
        self.result_folder = 'result/'
        for i in range(0, self.n_factor):
            self.factor["factor" + str(i)].append(point[i])
            self.result_folder = self.result_folder + f'{point[i]:.1f}_'
        
        self.alpha = self.run_alpha(point)
      
        zip = self._evaluate_server()        
        zip.extractall(self.result_folder)
        print('\nCOMPLETE: Sim Results printed to ', self.result_folder, ' folder')

        fileread = './'+self.result_folder+'/alpha.inspnl'
        df_alpha = pd.read_csv(fileread, index_col=0, sep=',', dayfirst=True)
        # read the results
        # PnL (Profit and loss) summation of each day
        filewrite = './'+self.result_folder+ '/'
        df_alpha.to_csv(filewrite +  'alpha.csv')
        sum_instru = df_alpha.sum(axis=1).reset_index(name ='sum')
        sum_instru['cummu'] = sum_instru['sum'].cumsum()
        alpha_pnl = sum_instru['cummu'].iloc[-1]
        self.factor["PnL"].append(alpha_pnl)
        sum_instru.to_csv(filewrite + 'summation.csv')

        # cummulative value across dates / volatility 
        cum = df_alpha.cumsum()
        cum.to_csv(filewrite + 'cummulativesummation.csv')

        return float(alpha_pnl)

    def output_data(self):
        print(self.factor)
        output_df = pd.DataFrame(self.factor)
        output_df.to_csv('output_df.csv')              

if __name__ == '__main__':
    # parse the args
    alpha_opt = AlphaOpt(cache='JP_cache')
    alpha_opt.input_data()
    
    bb = rbfopt.RbfoptUserBlackBox(alpha_opt.n_factor, alpha_opt.low_bound, alpha_opt.up_bound,
                                    np.array(alpha_opt.var_type), alpha_opt.evaluate)
    settings = rbfopt.RbfoptSettings(max_evaluations=50)
    alg = rbfopt.RbfoptAlgorithm(settings, bb)
    objval, x, itercount, evalcount, fast_evalcount = alg.optimize()
    
    alpha_opt.output_data()
    
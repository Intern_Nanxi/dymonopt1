import configparser  # for loading keys from the config file
from datetime import datetime
#from lib_DI_SQLTools import SQLTools
import pandas as pd
import numpy as np

import sys
#sys.path.append('../ccxt/')
#sys.path.append('../data_writer/')
sys.path.append('..')

# import mapping to table names
from table_map import bar_dic

# import table_lookup capability
from table_lookup import SymbolLookup
from data_writer.engine.sqlengine import SQLEngine


class CCSymbol:
    
    # extract all details about the market during initialisation
    def __init__(self, symbol : str, market : str, exchange : str, family_type : str = ''):
        self.symbol : str = symbol
        self.family_type : str = family_type
        self.market : str = market   # spot, linear or inverse
        self.market_table : str = 'crypto' + '.' + bar_dic[exchange][self.market]  # full table name - handle the table_name mapping here            
        self.sql_str = f"SELECT timestamp_utc, <DATA> FROM {self.market_table} WHERE symbol = '{self.symbol}' AND timestamp_utc >= '<START>' AND timestamp_utc <= '<END>'"

        self.sql_str_interval = f"select timestamp_utc, <DATA> from (select * , rank() over (partition by symbol, contract_type order by timestamp_utc asc) as time_rank from {self.market_table} where symbol = '{self.symbol}' and timestamp_utc >= '<START>' and timestamp_utc <= '<END>' ) as ranked_table where (time_rank % <INTERVAL> = 1)"

        self.sql_str_interval_include = f"select timestamp_utc, <DATA> from (select * , rank() over (partition by symbol, contract_type order by timestamp_utc asc) as time_rank from {self.market_table} where symbol = '{self.symbol}' and timestamp_utc >= '<START>' and timestamp_utc <= '<END>' ) as ranked_table where (time_rank % <INTERVAL> = 1 or timestamp_utc = '<END>')"
        


        config = configparser.ConfigParser()
    #     config.read_file(open(cwd + 'configs.cfg'))
    #     config.read_file(open(os.path.join(cwd, 'configs.cfg')))
        config.read_file(open('../configs.cfg'))
        SCHEMA = config.get('SQL_SERVER', 'SCHEMA')
        SQL_SERVER = config.get('SQL_SERVER', 'SQL_SERVER')
        SQL_DATABASE = config.get('SQL_SERVER', 'SQL_DATABASE')
        SQL_UID = config.get('SQL_SERVER', 'SQL_UID')
        SQL_PWD = config.get('SQL_SERVER', 'SQL_PWD')        
        self.SQLEngine = SQLEngine(SQL_SERVER, SQL_DATABASE, SQL_UID, SQL_PWD)

        
    # market means SPOT, LINEAR or INVERSE
    # family_type means spot, inverse_perp, linear_front etc  # checks if linear of inverse
    
    def __str__(self):  # family type means spot, inverse_perp etc
        return f'{self.symbol}, {self.family_type}, {self.market}, {self.market_table}'
    
    def __repr__(self):
        return self.__str__()
    
    def returns(self, from_time : datetime, to_time : datetime
                , interval : int = 1, include_end : bool = True
               ) -> pd.DataFrame:
        
        df = self.prices(from_time, to_time, interval, include_end)
        df = df/df.iloc[0]
        df = df.rename(columns={'open_pr':'ret'})

        return df
    
    def prices(self, from_time : datetime , to_time : datetime
               , interval : int = 1, include_end : bool = True
              ) -> pd.DataFrame:
        
        if interval > 1:
            if include_end:
                sql_str = self.sql_str_interval_include
            else:
                sql_str = self.sql_str_interval
            sql_str = sql_str.replace('<INTERVAL>', f'{interval}')
        else:
            sql_str = self.sql_str
            
        sql_str = sql_str.replace('<DATA>', 'open_pr')      
        sql_str = sql_str.replace('<START>', from_time.strftime('%Y-%m-%d %H:%M'))
        sql_str = sql_str.replace('<END>', to_time.strftime('%Y-%m-%d %H:%M'))      

#        df = SQLTools(sql_query=sql_str).query()
        df = self.SQLEngine.read_query(sql_str)
        df = df.set_index('timestamp_utc')
        df = df.rename(columns={'open_pr':'px'})
        return df  
    
    def price_at(self, at_time : datetime) -> float:
        
        sql_str = self.sql_str        
        sql_str = sql_str.replace('<DATA>', 'open_pr')      
        sql_str = sql_str.replace('<START>', at_time.strftime('%Y-%m-%d %H:%M'))
        sql_str = sql_str.replace('<END>', at_time.strftime('%Y-%m-%d %H:%M'))      

#         df = SQLTools(sql_query=sql_str).query()
        df = self.SQLEngine.read_query(sql_str)
        return df['open_pr'][0]

    def returns_between(self, from_time : datetime, to_time : datetime) -> float:
        
        start_price = self.price_at(from_time)
        end_price = self.price_at(to_time)
        
        return end_price / start_price
    
    def volume_between(self, from_time: datetime, to_time: datetime) -> float:
        
        sql_str = f"SELECT sum([volume]) as volume FROM {self.market_table} where symbol = '{self.symbol}' and timestamp_utc >= '<START>' and timestamp_utc <= '<END>'"
        sql_str = sql_str.replace('<START>', from_time.strftime('%Y-%m-%d %H:%M'))
        sql_str = sql_str.replace('<END>', to_time.strftime('%Y-%m-%d %H:%M'))      
        
#         df = SQLTools(sql_query=sql_str).query()
        df = self.SQLEngine.read_query(sql_str)
        
        return df['volume'][0]
    
    def volumes(self, from_time : datetime , to_time : datetime
               , interval : int = 1, drop_end : bool = True) -> pd.DataFrame:
        
        if interval > 1:
            sql_str = f"select timestamp_utc, vol from (select *, SUM(volume) over (PARTITION by FLOOR(time_rank / {interval})) as vol from (select * , rank() over (partition by symbol, contract_type order by timestamp_utc asc) as time_rank from {self.market_table} where symbol = '{self.symbol}' and timestamp_utc >= '<START>' and timestamp_utc <= '<END>' ) as ranked_table ) as tt where time_rank % {interval} = 1 order by timestamp_utc asc"
        else:
            sql_str = f"select timestamp_utc, vol from (select *, SUM(volume) over (PARTITION by FLOOR(time_rank / {interval})) as vol from (select * , rank() over (partition by symbol, contract_type order by timestamp_utc asc) as time_rank from {self.market_table} where symbol = '{self.symbol}' and timestamp_utc >= '<START>' and timestamp_utc <= '<END>' ) as ranked_table ) as tt order by timestamp_utc asc"
            

        sql_str = sql_str.replace('<START>', from_time.strftime('%Y-%m-%d %H:%M'))
        sql_str = sql_str.replace('<END>', to_time.strftime('%Y-%m-%d %H:%M'))      
        
#         df = SQLTools(sql_query=sql_str).query()
        df = self.SQLEngine.read_query(sql_str)
        df = df.set_index('timestamp_utc')
        
        if drop_end:
            df = df[:-1]
        return df

    def dollar_volumes(self, from_time : datetime , to_time : datetime
               , interval : int = 1, drop_end : bool = True) -> pd.DataFrame:
        
                    
        df = self.volumes(from_time, to_time, interval, drop_end) 
        
        if self.market == 'INVERSE':
            df_prices = self.prices(from_time, to_time, interval, not drop_end)
            df['vol'] = df['vol'] * df_prices['px']

        return df

    
class CCSymbolFamily:
    
    def __init__(self, spot_symbol: str, exchange: str):  # map to internal symbol here
        
        #this should be dynamic from SQL Table (SecMaster)
        
        # store exchange
        self.exchange = exchange
        
        
        # generate family map by breaking down spot symbol, and store
        base, quote = spot_symbol.split('.')  # break down internal symbol
        sym_class = SymbolLookup(base.upper())
        self.FAMILY_MAP = sym_class.generate_family_map(exchange, quote.upper())  # store family map
        
        
        self.family = {}
        self.load_family(self.FAMILY_MAP)  # this enters the family_map for that symbol
        self.default_start = datetime(2021,4,4)
        self.default_end = datetime(2021,4,5)
        
        
        # deal with family map here
                

    def __getattr__(self, fam_type : str) -> CCSymbol:  # generates a CCSymbol Class
        return self.family[fam_type.upper()]        
        
    def load_family(self, family_map):
        
        for _type in family_map:
            symbol = family_map[_type]
            market = _type.split('_')[0]
            
            self.family[_type] = CCSymbol(symbol, market, self.exchange, _type)

        
    def family_returns(self, start_time: datetime, end_time : datetime
                           , interval : int = 1, include_end : bool = True
                          ) -> pd.DataFrame:

        
        list_df = []
        
        for inst in self.family.values():
            
            _sym = f'{inst.family_type}'
            _df = inst.returns(start_time, end_time, interval, include_end)
            _df = _df.rename(columns={'px':_sym})
            list_df.append(_df)
            
        df = pd.concat(list_df, axis=1).sort_values('timestamp_utc')
        return df

    def family_prices(self, start_time: datetime, end_time : datetime
                           , interval : int = 1, include_end : bool = True
                          ) -> pd.DataFrame:

        list_df = []
        
        for inst in self.family.values():
            
            _sym = f'{inst.family_type}'
            _df = inst.prices(start_time, end_time, interval, include_end)
            _df = _df.rename(columns={'px':_sym})
            list_df.append(_df)
            
        df = pd.concat(list_df, axis=1).sort_values('timestamp_utc')
        return df
    

    def family_premiums(self, start_time: datetime, end_time : datetime
                           , interval : int = 1, include_end : bool = True
                          ) -> pd.DataFrame:
        

        df_prices = self.family_prices(start_time, end_time, interval, include_end)
        
        df = pd.DataFrame(index=df_prices.index)
        
        for col in df_prices.drop('SPOT',axis=1).columns:
            df[col] = df_prices[col] - df_prices['SPOT']
        
        return (df)
        
    def family_volumes(self, start_time: datetime, end_time : datetime
                           , interval : int = 1, drop_end : bool = True
                          ) -> pd.DataFrame:

        
        list_df = []
        
        for inst in self.family.values():
            
            _sym = f'{inst.family_type}'
            _df = inst.volumes(start_time, end_time, interval, drop_end)
            _df = _df.rename(columns={'vol':_sym})
            list_df.append(_df)
            
        df = pd.concat(list_df, axis=1).sort_values('timestamp_utc')
        return df            

    def family_dollar_volumes(self, start_time: datetime, end_time : datetime
                           , interval : int = 1, drop_end : bool = True
                          ) -> pd.DataFrame:

        
        list_df = []
        
        for inst in self.family.values():
            
            _sym = f'{inst.family_type}'
            _df = inst.dollar_volumes(start_time, end_time, interval, drop_end)
            _df = _df.rename(columns={'vol':_sym})
            list_df.append(_df)
            
        df = pd.concat(list_df, axis=1).sort_values('timestamp_utc')
        return df      

    
if __name__ == '__main__':
    exchange = 'binance'
    btcfam = CCSymbolFamily('BTC.USDT', exchange)
    print(btcfam)
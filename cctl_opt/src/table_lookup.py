#import ccxt

import configparser  # for loading keys from the config file
import hmac
import hashlib
import datetime as dt
import time
import urllib
import pandas as pd
import json
import pyodbc

# get path to current directory
# import os
# cwd = os.getcwd()

# import files and functions from another directory
#import sys
#sys.path.append('..')  # append path to current directory

from data.ccxt_model import ExchangeModule

# load keys from config file - saved separately
config = configparser.ConfigParser()
# config.read_file(open(cwd + 'configs.cfg'))
# config.read_file(open(os.path.join(cwd,'../configs.cfg')))
config.read_file(open('../configs.cfg'))

# import SQLEngine
from data_writer.engine.sqlengine import SQLEngine

# generate symbol family and generate a dictionary
# OHLCV table contains only exchange_symbols

# generate ccxt symbols for each symbol family - to revolve around the symbol
class SymbolLookup:
    
    # SERVER CONFIGS
    config = configparser.ConfigParser()
#     config.read_file(open(cwd + 'configs.cfg'))
#     config.read_file(open(os.path.join(cwd, 'configs.cfg')))
    config.read_file(open('../configs.cfg'))
    SCHEMA = config.get('SQL_SERVER', 'SCHEMA')
    SQL_SERVER = config.get('SQL_SERVER', 'SQL_SERVER')
    SQL_DATABASE = config.get('SQL_SERVER', 'SQL_DATABASE')
    SQL_UID = config.get('SQL_SERVER', 'SQL_UID')
    SQL_PWD = config.get('SQL_SERVER', 'SQL_PWD')
    
    # provide table name
#     symbol_master = 'ccxt_symbol_master'
    symbol_master = 'symbol_master'
    
    # set quote asset as USDT by default
    def __init__(self, spot_symbol: str):
        # initialise sqlengine
        self.SQLEngine = SQLEngine(self.SQL_SERVER, self.SQL_DATABASE, self.SQL_UID, self.SQL_PWD)
        
        # generate subset of data to read off from, and store
        query_string = f"SELECT * FROM {self.SCHEMA}.{self.symbol_master} WHERE BASE = '{spot_symbol}'"
        exclude_exotics = " AND INSTRUMENT_TYPE NOT IN ('option')"
        exclude_move = " AND CONTRACT_PRETYPE NOT IN ('move')"
        full_string = query_string + exclude_exotics + exclude_move
        
        # construct df
        self.symbol_df = self.SQLEngine.read_query(full_string)
        
    # generate symbol family from exchange - generate a dictionary of internal symbols
    def get_family(self, exchange: str, quote: str = 'USD') -> pd.DataFrame:
        """
        For the mentioned symbol, generate the following exchange symbols
        SPOT, INVERSE_PERP, INVERSE_FRONT, INVERSE_BACK, LINEAR_PERP, LINEAR_FRONT, LINEAR_BACK
        """
        # matching based off ccxt_symbol *key() from markets
        sym_df = self.symbol_df
        
        # quote filter
        #if quote == 'USD':
        #    quote_filter = ['USD', 'USDT', 'BUSD', 'USDC', 'USDS']
        #else:
        #    quote_filter = [quote]
        
        quote_filter = [quote]
        
        # filter by exchange and quote
        main_df = sym_df[(sym_df['exchange'] == exchange) & (sym_df['quote'].isin(quote_filter))]
        
        # always include inverse future
        inv_df = sym_df[(sym_df['exchange'] == exchange) & (sym_df['instrument_type'] == 'inverse_future')]
        
        output_df = main_df.append(inv_df)
        return output_df

    
    # generate dictionary from exchange
    def get_family_dic(self, exchange: str, quote: str = 'USDT') -> dict:        
        df = self.get_family(exchange, quote)
        df.reset_index(drop=True, inplace=True)
        
        # generate id
        cols = ['instrument_type', 'contract_type']
        df['id'] = df[cols].apply(lambda x: '-'.join(x.dropna().astype(str)), axis=1)  # using this for identification
        
        # build dictionary - construct using quote-base-instrument_type-contract_type
        dic = {}
        for i in df.index:
            dic[df.loc[i, 'id']] = df.loc[i, 'ccxt_symbol']
            
        return dic
    
    
    # resolve into traditional terminology
    def generate_family_map(self, exchange: str, quote: str = 'USDT') -> dict:
        """
        After generating the relevant dictionary, ensure correct terminology
        SPOT, INVERSE_PERP, INVERSE_FRONT, INVERSE_BACK, LINEAR_PERP, LINEAR_FRONT, LINEAR_BACK
        """
        
        dic = self.get_family_dic(exchange, quote)
        
        # rebuild dictionary
        term_map = {'spot-spot': 'SPOT',
                    'linear_future-perpetual': 'LINEAR_PERP',
                    'linear_future-current_quarter': 'LINEAR_FRONT',
                    'linear_future-next_quarter': 'LINEAR_BACK',
                    'linear_future-current_week': 'LINEAR_CURRENT_WEEK',
                    'linear_future-next_week': 'LINEAR_NEXT_WEEK',
                    'inverse_future-perpetual': 'INVERSE_PERP',
                    'inverse_future-current_quarter': 'INVERSE_FRONT',
                    'inverse_future-next_quarter': 'INVERSE_BACK',
                    'inverse_future-current_week': 'INVERSE_CURRENT_WEEK',
                    'inverse_future-next_week': 'INVERSE_NEXT_WEEK'
                   }
        
        # dictionary comprehension
        output_map = {}
        for key, value in dic.items():
            try:
                output_map[term_map[key]] = value
                
            # handle cases for when mapping is not included
            except:  # if mapping not included
                output_map[key] = value
            
        return output_map
    
    
# example of  implementation
"""
spot_symbol = 'BTC'
exchange = 'binance'
quote_asset = 'USDT'  # spot in terms of USDT stablecoin

sym_class = SymbolLookup(spot_symbol)
sym_df = sym_class.symbol_df
family_map = sym_class.generate_family_map(exchange, quote_asset)

"""
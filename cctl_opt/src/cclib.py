from datetime import datetime

def today_str() -> str:
    return datetime.today().strftime('%Y%m%d')

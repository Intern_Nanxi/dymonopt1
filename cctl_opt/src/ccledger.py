
from datetime import datetime, timedelta
from lib_DI_SQLTools import SQLTools
import pandas as pd
import numpy as np
from ccom import CCOrder, CCOrderManager
from ccdata import CCSymbol, CCSymbolFamily

MARK_TIMES = [1, 5, 10, 30, 60, 180, 360, 720, 1440, 2880, 4320, 10080, 20160, 40320]

class CCLedger:

    def __init__(self):
        self.fill_list = []
#         self.aa = CCTrade(CCOrder())    

    def insert_fills(self, fill_list):
        self.fill_list = self.fill_list + fill_list
        
    
    def analyze_fills(self):
        
        self.pnl = {}
        self.returns = {}
            
        for fill in self.fill_list:
            
            pnl = {}
            returns = {}
            

            for minutes in MARK_TIMES:
                try:
                    pnl[minutes] = fill.order.symbol.price_at(fill.fill_time + timedelta(minutes=minutes)) -  fill.fill_price
                    returns[minutes] = pnl[minutes]/fill.fill_price
                except Exception as e:
                    pass

            print ('\n')
            
            
            self.pnl[fill] = pnl
            self.returns[fill] = returns
            
#             for minutes in returns:
#                 print (f'{minutes} -- {returns[minutes]:.4f}')
            
    def print_returns(self):
        for fill in self.returns:
            print(f'TRADE: \n{fill}')
            print ('RETURNS:')
            for mark in self.returns[fill]:
                print (f'{mark}:{self.returns[fill][mark]:.4f}', end=', ')
            print ('\n')

if __name__ == '__main__':
    
    om = CCOrderManager('mystrat')
    om.create_order(CCSymbol('BTC/USDT', 'SPOT', 'SPOT'), 'MKT', 'B', 1.2, None)
    fl = om.send_orders(fill_time = datetime(2021,4,4))
    
    om.create_order(CCSymbol('ETH/USDT', 'SPOT', 'SPOT'), 'MKT', 'B', 2, None)
    fl = om.send_orders(fill_time = datetime(2021,4,4,1))
    
    om.create_order(CCSymbolFamily('BTC/USDT').linear_perp, 'MKT', 'B', 2.5, None)
    fl = om.send_orders(fill_time = datetime(2021,4,4,1,2))

    
    ledger = CCLedger()
    ledger.insert_fills(fl)
    ledger.analyze_fills()
    ledger.print_returns()

#     _ccom.create_order('ETH/USDT', 'LINEAR', 'Binance', 'MKT', 'S', 3, None)
#     _ccom.create_order('SHIBUSD', 'SPOT', 'Binance', 'MKT', 'B', 0.2, None)
#     _ccom.create_order('BTCUSD', 'SPOT', 'Binance', 'MKT', 'B', 1.2, None)
#     print(str(o1))
#     _ccom.print_orders()


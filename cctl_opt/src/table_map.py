# Mapping between Exchange and Table Name in Data Warehouse

bar_dic = {'binance': {'SPOT': 'binance_spot_bars',
                       'LINEAR': 'binance_usdt_bars',
                       'INVERSE': 'binance_coinm_bars'
                      },
           
           'ftx': {'ALL': 'ftx_bars'},
           
           'okex': {'ALL': 'ccxt_okex_bars'},
           
           'coinbase': {'ALL': 'ccxt_coinbase_bars'},
           
           'huobi': {'ALL': 'ccxt_huobi_bars'}
          }


# bar_dic = {'binance': {'SPOT': 'ccxt_spot_bars',
#                        'LINEAR': 'ccxt_usdt_bars',
#                        'INVERSE': 'ccxt_coinm_bars'
#                       },
           
#            'ftx': {'ALL': 'ccxt_ftx_bars'},
           
#            'okex': {'ALL': 'ccxt_okex_bars'},
           
#            'coinbase': {'ALL': 'ccxt_coinbase_bars'},
           
#            'huobi': {'ALL': 'ccxt_huobi_bars'}
#           }
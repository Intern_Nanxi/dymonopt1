import requests
import os
import zipfile
import io
import pandas as pd
import json
import paramiko


class BtServer():
    def __init__(self):
        #self.server='http://192.168.2.58:5555/' #KC IP address
        #self.server = 'http://10.5.0.13:5555/'  # DevBox IP
        self.server = 'http://10.5.0.12:5555/'  # ProdBox IP

        self.sftp_host = '10.5.0.12'
        self.sftp_port = 22
        self.sftp_user = 'BtUser'
        self.sftp_pw = 'btcache111'

    def evaluate_alphafile(self, filedir = r'bt\alpha\alphamixer_CH',
                       filename='alpha.signal'):
        '''
        Evaluates with input as flat file
        Columns: ID_{QAID}
        Index: Date

        Returns:
            zipfile
        '''

        file = os.path.join(filedir, filename)

        #Verifying input format
        df = pd.read_csv(file, index_col=0, parse_dates=True)
        if not all([x.startswith('ID_') for x in df.columns]):
            return 'Error:Incorrect dataframe format. Columns should be ID_{QAID}'
        if not isinstance(df.index, pd.DatetimeIndex):
            return 'Error: Incorrect dataframe index. Index should be dates'

        fileDict = {'file':open(file, 'rb')}
        res = requests.post(self.server+'eval', files=fileDict)

        z = zipfile.ZipFile(io.BytesIO(res.content))
        z.extractall(filename)
        print(filename, ' Done')

    def evaluate_alphadf(self, df):
        '''
        Evaluates with input as pandas dataframe
        Columns: ID_{QAID}
        Index: Date

        Returns:
            zipfile
        '''
        if not all([x.startswith('ID_') for x in df.columns]):
            return 'Error:Incorrect dataframe format. Columns should be ID_{QAID}'
        if not isinstance(df.index, pd.DatetimeIndex):
            return 'Error: Incorrect dataframe index. Index should be dates'

        df.index.name = 'MarketDate'
        df.index = df.index.astype(str)
        df.reset_index(inplace=True)
        dfJson = df.to_dict('list')
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        res = requests.post(self.server+'evaldf',data=json.dumps(dfJson), headers=headers)
        z = zipfile.ZipFile(io.BytesIO(res.content))
        return z

    def syncCache_SFTP(self, cache, filename=None):
        transport = paramiko.Transport((self.sftp_host, self.sftp_port))
        transport.connect(None, self.sftp_user, self.sftp_pw)
        sftp = paramiko.SFTPClient.from_transport(transport)

        filepath = 'cache/'+cache+'/'
        localpath = os.path.join(os.getcwd(), cache)
        if not os.path.exists(localpath):
            os.makedirs(localpath)

        if filename is None:
            #All files in cache
            for file in sftp.listdir(filepath):
                sftp.get(filepath+'/'+file, os.path.join(os.getcwd(), cache, file))
                print(file, ' Done')
        else:
            #Specific file in cache
            sftp.get(filepath + '/' + filename, os.path.join(os.getcwd(), cache, filename))
        print('SFTP Download Done')

    def getCache(self, cache, filename):
        transport = paramiko.Transport((self.sftp_host, self.sftp_port))
        transport.connect(None, self.sftp_user, self.sftp_pw)
        sftp = paramiko.SFTPClient.from_transport(transport)

        filepath = 'cache/' + cache + '/'
        if filename.endswith('.series'):
            with sftp.open(filepath + '/' + filename, 'rb') as fl:
                df = pd.read_csv(fl, header=None,parse_dates=True)
        else:
            with sftp.open(filepath + '/' + filename, 'rb') as fl:
                df = pd.read_csv(fl,index_col=0,parse_dates=True)
        print(filename, ' Downloaded')
        return df

import pdb

from numpy import arange
import itertools
import pandas as pd
import ast
from btalpha_try import AlphaSample

import sys
sys.path.insert(0, '/opt/conda/envs/opt/lib/python3.8/site-packages')

import rbfopt
import numpy as np
import rbfopt.rbfopt_black_box as bb

settings = rbfopt.RbfoptSettings(minlp_solver_path='/opt/conda/envs/opt/lib')
 

class AlphaOpt(AlphaSample):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)              
        self.n_factor : int = 0
        self.nreal : int = 0
        self.nint : int = 0
    
    def input_file(self):
        input_factor= pd.read_csv('input.txt', sep=', ', converters={'value':ast.literal_eval}, engine='python')

        self.low_list = []
        self.up_list = []
        self.factor_dict : Dict[str, pd.DataFrame] = {"Factor":[], "Value":[]}
   
        for index, row in input_factor.iterrows():
            value = row['value']
            factor = row['factor']

            if value[0] == '(':
                value = value[1:-1]
                value = value.split(',')
                if row['type'] == 'float':
                    value_min = float(value[0])
                    value_max = float(value[1])
                    value_step = float(value[2])
                    self.low_list.append(value_min)
                    self.up_list.append(value_max)
                    self.nreal += 1
                elif row['type'] == 'int':
                    value_min = int(value[0])
                    value_max = int(value[1])
                    value_step = int(value[2])
                    self.low_list.append(value_min)
                    self.up_list.append(value_max)
                    self.nint += 1
                value_list = [*arange(value_min, value_max, value_step)]
            elif value[0] == '[':
                value = value[1:-1]
                value = value.split(',')
                if row['type'] == 'float':
                    value_list = [float(x) for x in value]
                    self.low_list.append(value_list[0])
                    self.up_list.append(value_list[-1])
                    self.nreal += 1
                elif row['type'] == 'int':
                    value_list = [int(x) for x in value]
                    self.low_list.append(value_list[0])
                    self.up_list.append(value_list[-1])
                    self.nint += 1
                #value_list = ast.literal_eval(value)
            self.factor_dict["Factor"].append(factor)
            self.factor_dict["Value"].append(value_list)
        self.n_factor = self.nint + self.nreal
        print(f'factor dictionary{self.factor_dict}, factor numbers{self.n_factor}, upper bound{self.up_list}, lower bound{self.low_list}')        
        self.factor_t1, self.factor_t2, self.factor_t3, self.factor_q1, self.factor_q2, self.pnl = ([] for i in range(self.n_factor+1))
        
    def evaluate(self, point : list):
        print("point in evaluate", point)
        self.factor_t1.append(point[0])
        self.factor_t2.append(point[1])
        self.factor_t3.append(point[2])
        self.factor_q1.append(point[3])
        self.factor_q2.append(point[4])
        
        self.alpha = self.run_alpha(point)
      
        zip = self._evaluate_server()
        self.result_folder = f'result/t1_{int(point[0]):1d}t2_{int(point[1]):1d}t3_{int(point[2]):1d}q1_{point[3]:.1f}q2_{point[4]:.1f}'
        zip.extractall(self.result_folder)
        print('\nCOMPLETE: Sim Results printed to ', self.result_folder, ' folder')

        fileread = './'+self.result_folder+'/alpha.inspnl'
        df_alpha = pd.read_csv(fileread, index_col=0, sep=',', dayfirst=True)
        # read the results
        # PnL (Probable and loss) summation of each day
        filewrite = './'+self.result_folder+ '/'
        df_alpha.to_csv(filewrite +  'alpha.csv')
        sum_instru = df_alpha.sum(axis=1).reset_index(name ='sum')
        sum_instru['cummu'] = sum_instru['sum'].cumsum()
        alpha_pnl = sum_instru['cummu'].iloc[-1]
        self.pnl.append(alpha_pnl)
        print("PnL for each iteration:", self.pnl)
        sum_instru.to_csv(filewrite + 'summation.csv')

        # cummulative value across dates / volatility 
        cum = df_alpha.cumsum()
        cum.to_csv(filewrite + 'cummulativesummation.csv')

        return float(alpha_pnl)


    def output_file(self):
        datalist = [self.factor_t1, self.factor_t2, self.factor_t3, self.factor_q1, self.factor_q2, self.pnl]
        output_df = pd.DataFrame(datalist).transpose()
        # out_scatter = pd.plotting.scatter_matrix(output_df)
        # plt.savefig(r"scatter_fig.png")
        output_df.columns = ['T1', 'T2', 'T3', 'Q1', 'Q2', 'PnL']
        output_df.to_csv('output_df.csv')
              

if __name__ == '__main__':
    # parse the args
    alpha_opt = AlphaOpt(cache='JP_cache')
    alpha_opt.input_file()
    
    bb = rbfopt.RbfoptUserBlackBox(alpha_opt.n_factor, alpha_opt.low_list, alpha_opt.up_list,
                                    np.array(['I', 'I', 'I', 'R', 'R']), alpha_opt.evaluate)
    settings = rbfopt.RbfoptSettings(max_evaluations=6)
    alg = rbfopt.RbfoptAlgorithm(settings, bb)
    objval, x, itercount, evalcount, fast_evalcount = alg.optimize()
    print(x, objval)
    
    alpha_opt.output_file()
    
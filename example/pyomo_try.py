import pdb
import os
from os.path import abspath, dirname
pyomodir = dirname(dirname(dirname(dirname(abspath(__file__)))))
pyomodir += os.sep
currdir = dirname(abspath(__file__))+os.sep
 
# import pyutilib.th as unittest
import pyutilib.services
import pyomo.environ
# import sys
 
import pyomo.opt
import pyomo.opt.blackbox
 
# old_tempdir = pyutilib.services.TempfileManager.tempdir


def plus_3(r1, i1, i2, i3):
    r = r1
    i = i1 + i2 + i3
    result = r + i
    print(result)

    return result


class AlphaTest(pyomo.opt.blackbox.MixedIntOptProblem):
    def __init__(self):
        pyomo.opt.blackbox.MixedIntOptProblem.__init__(self)
        #self.real_lower=[0.3, 0.1, 0.2]
        #self.real_upper=[0.6, 0.6, 0.6]
        #self.int_lower=[10]*2
        #self.int_upper=[13]*2
        
        #self.nreal=3
        #self.nint=2
        
        self.real_lower=[0.3, 0.1]
        self.real_upper=[0.4,0.2]
        self.int_lower=[10, 11, 13]
        self.int_upper=[12, 13, 15]
        self.nint=3
        self.nreal=2
        
    def function_value(self, point):
        # validate initial points from XML file
        self.validate(point)
#         self.alpha = self.run_alpha()
        # return point.reals[0] + point.ints[0] + point.ints[1]
        return plus_3(point.reals[0], point.ints[0], point.ints[1], point.ints[2])


# class TestColinMain(unittest.TestCase):
 
#     @classmethod
#     def setUpClass(cls):
#         import pyomo.environ
 
#     def setUp(self):
#         self.do_setup(False)
#         pyutilib.services.TempfileManager.tempdir = currdir
 
#     def do_setup(self,flag):
#         pyutilib.services.TempfileManager.tempdir = currdir
#         self.ps = pyomo.opt.SolverFactory('ps')
#         self.problem=AlphaTest()
 
#     def tearDown(self):
#         pyutilib.services.TempfileManager.clear_tempfiles()
#         pyutilib.services.TempfileManager.tempdir = old_tempdir
 
#     def test_main(self):
#         self.problem.main(['test_main', currdir+'pyomo_try.xml', currdir+'pyomoresults1.out'])
#         self.assertMatchesXmlBaseline(currdir+'results1.out', currdir+'results1.xml', tolerance=0.01, exact=True)

if __name__ == "__main__":
#     unittest.main()
    ps = pyomo.opt.SolverFactory('ps')
    problem = AlphaTest()
    problem.main(['test_main', currdir+'pyomo_try.xml', currdir+'pyomoresults1.out'])
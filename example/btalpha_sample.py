import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from btalpha_sim import BtAlpha_Sim
import pandas as pd

_T1 : int = 10   # [10:15:1]
_T2 : int = 15 
_T3 : int = 15
_Q1 : float = 0.3  #(0.1, 0.5, 0.01)
_Q2 : float = 0.4
#s.t. _Q1 + Q2 <= 1

class AlphaSample(BtAlpha_Sim):
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.result_folder = 'result'

    def run_alpha(self):
        '''                                                                                                                                            
        write alpha and assign                                                                                                                         
        '''
        # Define Alpha                                                                                                             

        alpha1 = self.df.open.apply(lambda x: -1 * self.df.open[x.name].rolling(_T1).corr(self.df.vwap[x.name]), axis=0)
        alpha2 = self.df.open.apply(lambda x: -1 * self.df.open[x.name].rolling(_T2).corr(self.df.vwap[x.name]), axis=0)
        alpha3 = self.df.open.apply(lambda x: -1 * self.df.open[x.name].rolling(_T3).corr(self.df.vwap[x.name]), axis=0)
        alpha = _Q1 * alpha1 + _Q2 * alpha2 + (1-_Q1-_Q2) * alpha3
        return alpha

if __name__ == '__main__':
    alpha = AlphaSample(cache='JP_cache')
#     alpha.result_folder = 'results'
    alpha.evaluate()

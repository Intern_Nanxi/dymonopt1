from numpy import arange
import itertools
import pandas as pd
import ast
from btalpha_try import AlphaSample


class OptAlpha(AlphaSample):
    def input_file(self):
        input_factor= pd.read_csv('input.txt', sep=', ', converters={'value':ast.literal_eval}, engine='python')
        self.factor_list = []
        for index, row in input_factor.iterrows():
            value = row['value']
            if value[0] == '(':
                value = value[1:-1]
                value = value.split(',')
                if row['type'] == 'float':
                    value_min = float(value[0])
                    value_max = float(value[1])
                    value_step = float(value[2])
                elif row['type'] == 'int':
                    value_min = int(value[0])
                    value_max = int(value[1])
                    value_step = int(value[2])
                self.factor_list.append([*arange(value_min, value_max, value_step)])
            elif value[0] == '[':
                value = value[1:-1]
                value = value.split(',')
                if row['type'] == 'float':
                    value_list = [float(x) for x in value]
                elif row['type'] == 'int':
                    value_list = [int(x) for x in value]
                #value_list = ast.literal_eval(value)
                self.factor_list.append(value_list)

        print(self.factor_list)

    def evaluate(self):
        factor_t1, factor_t2, factor_t3, factor_q1, factor_q2, self.pnl = ([] for i in range(6))
        for self.t1,self.t2,self.t3,self.q1,self.q2 in itertools.product(self.factor_list[0], self.factor_list[1], self.factor_list[2], self.factor_list[3], self.factor_list[4]):                               
            factor_t1.append(self.t1)
            factor_t2.append(self.t2)
            factor_t3.append(self.t3)
            factor_q1.append(self.q1)
            factor_q2.append(self.q2)
            # change the formal parameters of run_alpha function in btalpha_try
            self.alpha = self.run_alpha(self.t1,self.t2,self.t3,self.q1,self.q2)
      
            zip = self._evaluate_server()
            self.result_folder = f'result/t1_{self.t1}t2_{self.t2}t3_{self.t3}q1_{self.q1:.1f}q2_{self.q2:.1f}'
            zip.extractall(self.result_folder)
            print('\nCOMPLETE: Sim Results printed to ', self.result_folder, ' folder')
            alpha.output_file()
        print(factor_t1,factor_t2,factor_t3,factor_q1,factor_q2)
        datalist = [factor_t1, factor_t1, factor_t3, factor_q1, factor_q2, self.pnl]
        output_df = pd.DataFrame(datalist).transpose()
        # out_scatter = pd.plotting.scatter_matrix(output_df)
        # plt.savefig(r"scatter_fig.png")
        output_df.columns = ['T1', 'T2', 'T3', 'Q1', 'Q2', 'PnL']
        output_df.to_csv('output_df.csv')


    def output_file(self):
        fileread = './'+self.result_folder+'/alpha.inspnl'
        df_alpha = pd.read_csv(fileread, index_col=0, sep=',', dayfirst=True)
        # read the results
        # PnL (Probable and loss) summation of each day
        filewrite = './'+self.result_folder+ '/'
        df_alpha.to_csv(filewrite +  'alpha.csv')
        sum_instru = df_alpha.sum(axis=1).reset_index(name ='sum')
        sum_instru['cummu'] = sum_instru['sum'].cumsum()
        self.alpha_pnl = sum_instru['cummu'].iloc[-1]
        self.pnl.append(self.alpha_pnl)
        print("PnL for each iteration:", self.pnl)
        sum_instru.to_csv(filewrite + 'summation.csv')

        # cummulative value across dates / volatility 
        cum = df_alpha.cumsum()
        cum.to_csv(filewrite + 'cummulativesummation.csv')
        

if __name__ == '__main__':
    # parse the args
    
    alpha = OptAlpha(cache='JP_cache')
    alpha.input_file()
#     alpha.result_folder = 'results'
    alpha.evaluate()
    
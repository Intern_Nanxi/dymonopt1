import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="btalpha",
    version="0.0.1",
    author="Kenneth Chua",
    author_email="kenneth.chua@dymonasia.com",
    description="AlphaEval",
    long_description=long_description,
    url="https://gitlab.com/kenC_/btalpha",
    packages=setuptools.find_packages(exclude=('tests')),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
import sys
sys.path.insert(0, '/opt/conda/envs/opt/lib/python3.8/site-packages')

import rbfopt
import numpy as np
import rbfopt.rbfopt_black_box as bb


settings = rbfopt.RbfoptSettings(minlp_solver_path='/opt/conda/envs/opt/lib',nlp_solver_path='/opt/conda/envs/opt/lib/python3.8/site-packages/ipopt')

def obj_funct(x):
    r = x[0]
    i = x[1] + x[2] + x[3]
    result = r + i
    print(result)

    return result


# def obj_funct(x):
# return x[0]*x[1] - x[2]

bb = rbfopt.RbfoptUserBlackBox(4, np.array([0] * 4), np.array([10] * 4),
                                np.array(['R', 'R', 'I', 'I']), obj_funct)
settings = rbfopt.RbfoptSettings(max_evaluations=50)
alg = rbfopt.RbfoptAlgorithm(settings, bb)
objval, x, itercount, evalcount, fast_evalcount = alg.optimize()
print(x, objval)